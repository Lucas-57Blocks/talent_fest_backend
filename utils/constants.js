const mailTemplates = {
  SUBJECT: 'Job for you.',
  NOTIFY_BEFORE_CODE: `
    <p>Hello,</p>
    <p>This is your code `,
  NOTIFY_AFTER_CODE: ` for tracking the candidate's state.</p>`,
}

module.exports = { mailTemplates };
