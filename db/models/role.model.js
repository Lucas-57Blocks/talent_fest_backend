const { Model, DataTypes, Sequelize } = require('sequelize');

const ROLE_TABLE = 'roles';

const RoleSchema = {
  id: {
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: DataTypes.INTEGER
  },
  role: {
    allowNull: false,
    type: DataTypes.STRING
  },
  createdAt: {
    allowNull: false,
    type: DataTypes.DATE,
    field: 'created_at',
    defaultValue: Sequelize.NOW
  },
  isDeleted: {
    allowNull: false,
    type: DataTypes.BOOLEAN,
    field: 'is_deleted',
    defaultValue: false
  },
}

class Role extends Model {
  static assocciate(models) {
    this.hasOne(models.User, {
      as: 'user',
      foreignKey: 'roleId'
    })
  }

  static config(sequelize){
    return {
      sequelize,
      tableName: ROLE_TABLE,
      modelName: 'Role',
      timestamps: false
    }
  }
}

module.exports = { ROLE_TABLE, RoleSchema, Role };
