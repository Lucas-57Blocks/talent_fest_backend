const { Model, DataTypes, Sequelize } = require('sequelize');
const { REFERRAL_STATUS_TABLE } = require('./referral.status.model');

const REFERRAL_TABLE = 'referrals';

const ReferralSchema = {
  id: {
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: DataTypes.INTEGER
  },
  code: {
    allowNull: false,
    type: DataTypes.STRING,
  },
  candidateName: {
    allowNull: false,
    field: 'candidate_name',
    type: DataTypes.STRING
  },
  candidateEmail: {
    allowNull: false,
    field: 'candidate_email',
    type: DataTypes.STRING
  },
  referrerName: {
    allowNull: false,
    field: 'referrer_name',
    type: DataTypes.STRING
  },
  referrerEmail: {
    allowNull: false,
    field: 'referrer_email',
    type: DataTypes.STRING
  },
  referralStatusId: {
    allowNull: false,
    field: 'referral_status_id',
    type: DataTypes.INTEGER,
    reference: {
      model: REFERRAL_STATUS_TABLE,
      key: 'id'
    },
    onUpdate: 'CASCADE',
    onDelete: 'SET NULL'
  },
  createdAt: {
    allowNull: false,
    type: DataTypes.DATE,
    field: 'created_at',
    defaultValue: Sequelize.NOW
  },
  isDeleted: {
    allowNull: false,
    type: DataTypes.BOOLEAN,
    field: 'is_deleted',
    defaultValue: false
  },
}

class Referral extends Model {
  static assocciate(models) {
    this.belongsTo(models.ReferralStatus, { as: 'referral_status' });
  }

  static config(sequelize){
    return {
      sequelize,
      tableName: REFERRAL_TABLE,
      modelName: 'Referral',
      timestamps: false
    }
  }
}

module.exports = { REFERRAL_TABLE, ReferralSchema, Referral };
