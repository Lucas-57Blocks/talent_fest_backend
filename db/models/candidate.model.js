const { Model, DataTypes, Sequelize } = require('sequelize');
const { JOB_TABLE } = require('./job.model');
const { ENGLISH_LEVEL_TABLE } = require('./english.level.model');
const { CANDIDATE_STATUS_TABLE } = require('./candidate.status.model');

const CANDIDATE_TABLE = 'candidates';

const CandidateSchema = {
  id: {
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: DataTypes.INTEGER
  },
  jobId: {
    allowNull: false,
    field: 'job_id',
    type: DataTypes.INTEGER,
    reference: {
      model: JOB_TABLE,
      key: 'id'
    },
    onUpdate: 'CASCADE',
    onDelete: 'SET NULL'
  },
  name: {
    allowNull: false,
    type: DataTypes.STRING,
  },
  email: {
    allowNull: false,
    type: DataTypes.STRING,
    unique: true
  },
  phone: {
    allowNull: false,
    type: DataTypes.STRING,
    unique: true
  },
  linkedIn: {
    allowNull: false,
    field: 'linked_in',
    type: DataTypes.STRING,
    unique: true
  },
  englishLevelId: {
    allowNull: false,
    field: 'english_level_id',
    type: DataTypes.INTEGER,
    reference: {
      model: ENGLISH_LEVEL_TABLE,
      key: 'id'
    },
    onUpdate: 'CASCADE',
    onDelete: 'SET NULL'
  },
  resume: {
    allowNull: true,
    type: DataTypes.STRING
  },
  reasonsWhy: {
    allowNull: true,
    type: DataTypes.STRING
  },
  candidateStatusId: {
    allowNull: false,
    field: 'candidate_status_id',
    type: DataTypes.INTEGER,
    reference: {
      model: CANDIDATE_STATUS_TABLE,
      key: 'id'
    },
    onUpdate: 'CASCADE',
    onDelete: 'SET NULL'
  },
  createdAt: {
    allowNull: false,
    type: DataTypes.DATE,
    field: 'created_at',
    defaultValue: Sequelize.NOW
  },
  isDeleted: {
    allowNull: false,
    type: DataTypes.BOOLEAN,
    field: 'is_deleted',
    defaultValue: false
  },
}

class Candidate extends Model {
  static assocciate(models) {
    this.belongsTo(models.Job, { as: 'job' });
    this.belongsTo(models.EnglishLevel, { as: 'english_level' });
    this.belongsTo(models.CandidateStatus, { as: 'candidate_status' });

  }

  static config(sequelize){
    return {
      sequelize,
      tableName: CANDIDATE_TABLE,
      modelName: 'Candidate',
      timestamps: false
    }
  }
}

module.exports = { CANDIDATE_TABLE, CandidateSchema, Candidate };
