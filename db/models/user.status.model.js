const { Model, DataTypes, Sequelize } = require('sequelize');

const USER_STATUS_TABLE = 'user_status';

const UserStatusSchema = {
  id: {
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: DataTypes.INTEGER
  },
  status: {
    allowNull: false,
    type: DataTypes.STRING,
  },
  createdAt: {
    allowNull: false,
    type: DataTypes.DATE,
    field: 'created_at',
    defaultValue: Sequelize.NOW
  },
  isDeleted: {
    allowNull: false,
    type: DataTypes.BOOLEAN,
    field: 'is_deleted',
    defaultValue: false
  },
}

class UserStatus extends Model {
  static assocciate(models) {
    this.hasOne(models.User, {
      as: 'user',
      foreignKey: 'userStatusId'
    })
  }

  static config(sequelize){
    return {
      sequelize,
      tableName: USER_STATUS_TABLE,
      modelName: 'UserStatus',
      timestamps: false
    }
  }
}

module.exports = { USER_STATUS_TABLE, UserStatusSchema, UserStatus };
