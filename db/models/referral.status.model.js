const { Model, DataTypes, Sequelize } = require('sequelize');

const REFERRAL_STATUS_TABLE = 'referral_status';

const ReferralStatusSchema = {
  id: {
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: DataTypes.INTEGER
  },
  status: {
    allowNull: false,
    type: DataTypes.STRING,
  },
  createdAt: {
    allowNull: false,
    type: DataTypes.DATE,
    field: 'created_at',
    defaultValue: Sequelize.NOW
  },
  isDeleted: {
    allowNull: false,
    type: DataTypes.BOOLEAN,
    field: 'is_deleted',
    defaultValue: false
  },
}

class ReferralStatus extends Model {
  static assocciate(models) {
    this.hasOne(models.Referral, {
      as: 'referral',
      foreignKey: 'referralStatusId'
    })
  }

  static config(sequelize){
    return {
      sequelize,
      tableName: REFERRAL_STATUS_TABLE,
      modelName: 'ReferralStatus',
      timestamps: false
    }
  }
}

module.exports = { REFERRAL_STATUS_TABLE, ReferralStatusSchema, ReferralStatus };
