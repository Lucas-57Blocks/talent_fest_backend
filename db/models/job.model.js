const { Model, DataTypes, Sequelize } = require('sequelize');

const JOB_TABLE = 'jobs';

const JobSchema = {
  id: {
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: DataTypes.INTEGER
  },
  name: {
    allowNull: false,
    type: DataTypes.STRING,
  },
  summary: {
    allowNull: false,
    type: DataTypes.STRING
  },
  jobDescription: {
    allowNull: false,
    field: 'job_description',
    type: DataTypes.STRING
  },
  jobDescriptionWishes: {
    allowNull: false,
    field: 'job_description_wishes',
    type: DataTypes.STRING
  },
  jobOffer: {
    allowNull: false,
    field: 'job_offer',
    type: DataTypes.STRING
  },
  createdAt: {
    allowNull: false,
    type: DataTypes.DATE,
    field: 'created_at',
    defaultValue: Sequelize.NOW
  },
  isHidden: {
    allowNull: false,
    type: DataTypes.BOOLEAN,
    field: 'is_deleted',
    defaultValue: false
  },
  isDeleted: {
    allowNull: false,
    type: DataTypes.BOOLEAN,
    field: 'is_deleted',
    defaultValue: false
  },
}

class Job extends Model {
  static assocciate(models) {
    this.hasOne(models.Candidate, {
      as: 'candidate',
      foreignKey: 'jobId'
    })
  }

  static config(sequelize){
    return {
      sequelize,
      tableName: JOB_TABLE,
      modelName: 'Job',
      timestamps: false
    }
  }
}

module.exports = { JOB_TABLE, JobSchema, Job };
