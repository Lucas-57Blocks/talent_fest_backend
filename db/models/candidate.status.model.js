const { Model, DataTypes, Sequelize } = require('sequelize');

const CANDIDATE_STATUS_TABLE = 'candidate_status';

const CandidateStatusSchema = {
  id: {
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: DataTypes.INTEGER
  },
  status: {
    allowNull: false,
    type: DataTypes.STRING,
  },
  createdAt: {
    allowNull: false,
    type: DataTypes.DATE,
    field: 'created_at',
    defaultValue: Sequelize.NOW
  },
  isDeleted: {
    allowNull: false,
    type: DataTypes.BOOLEAN,
    field: 'is_deleted',
    defaultValue: false
  },
}

class CandidateStatus extends Model {
  static assocciate(models) {
    this.hasOne(models.Candidate, {
      as: 'candidate',
      foreignKey: 'candidateStatusId'
    })
  }

  static config(sequelize){
    return {
      sequelize,
      tableName: CANDIDATE_STATUS_TABLE,
      modelName: 'CandidateStatus',
      timestamps: false
    }
  }
}

module.exports = { CANDIDATE_STATUS_TABLE, CandidateStatusSchema, CandidateStatus };
