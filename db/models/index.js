const { UserStatus, UserStatusSchema } = require('./user.status.model');
const { ReferralStatus, ReferralStatusSchema } = require('./referral.status.model');
const { CandidateStatus, CandidateStatusSchema } = require('./candidate.status.model');
const { EnglishLevel, EnglishLevelSchema } = require('./english.level.model');
const { Role, RoleSchema } = require('./role.model');
const { Job, JobSchema } = require('./job.model');
const { User, UserSchema } = require('./user.model');
const { Referral, ReferralSchema } = require('./referral.model');
const { Candidate, CandidateSchema } = require('./candidate.model');

function setupModels(sequelize) {
  UserStatus.init(UserStatusSchema, UserStatus.config(sequelize));
  ReferralStatus.init(ReferralStatusSchema, ReferralStatus.config(sequelize));
  CandidateStatus.init(CandidateStatusSchema, CandidateStatus.config(sequelize));
  EnglishLevel.init(EnglishLevelSchema, EnglishLevel.config(sequelize));
  Role.init(RoleSchema, Role.config(sequelize));
  Job.init(JobSchema, Job.config(sequelize));
  User.init(UserSchema, User.config(sequelize));
  Referral.init(ReferralSchema, Referral.config(sequelize));
  Candidate.init(CandidateSchema, Candidate.config(sequelize));

  UserStatus.assocciate(sequelize.models);
  ReferralStatus.assocciate(sequelize.models);
  CandidateStatus.assocciate(sequelize.models);
  EnglishLevel.assocciate(sequelize.models);
  Role.assocciate(sequelize.models);
  Job.assocciate(sequelize.models);
  User.assocciate(sequelize.models);
  Referral.assocciate(sequelize.models);
  Candidate.assocciate(sequelize.models);
}

module.exports = setupModels;
