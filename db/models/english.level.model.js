const { Model, DataTypes, Sequelize } = require('sequelize');

const ENGLISH_LEVEL_TABLE = 'english_level';

const EnglishLevelSchema = {
  id: {
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: DataTypes.INTEGER
  },
  englishLevel: {
    allowNull: false,
    field: 'english_level',
    type: DataTypes.STRING,
  },
  createdAt: {
    allowNull: false,
    type: DataTypes.DATE,
    field: 'created_at',
    defaultValue: Sequelize.NOW
  },
  isDeleted: {
    allowNull: false,
    type: DataTypes.BOOLEAN,
    field: 'is_deleted',
    defaultValue: false
  },
}

class EnglishLevel extends Model {
  static assocciate(models) {
    this.hasOne(models.Candidate, {
      as: 'candidate',
      foreignKey: 'englishLevelId'
    })
  }

  static config(sequelize){
    return {
      sequelize,
      tableName: ENGLISH_LEVEL_TABLE,
      modelName: 'EnglishLevel',
      timestamps: false
    }
  }
}

module.exports = { ENGLISH_LEVEL_TABLE, EnglishLevelSchema, EnglishLevel };
