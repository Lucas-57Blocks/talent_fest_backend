'use strict';

const { UserStatusSchema, USER_STATUS_TABLE } = require('./../models/user.status.model');
const { ReferralStatusSchema, REFERRAL_STATUS_TABLE } = require('./../models/referral.status.model');
const { CandidateStatusSchema, CANDIDATE_STATUS_TABLE } = require('./../models/candidate.status.model');
const { EnglishLevelSchema, ENGLISH_LEVEL_TABLE } = require('./../models/english.level.model');
const { RoleSchema, ROLE_TABLE } = require('./../models/role.model');
const { JobSchema, JOB_TABLE } = require('./../models/job.model');
const { UserSchema, USER_TABLE } = require('./../models/user.model');
const { ReferralSchema, REFERRAL_TABLE } = require('./../models/referral.model');
const { CandidateSchema, CANDIDATE_TABLE } = require('./../models/candidate.model');

module.exports = {
  async up (queryInterface) {
    await queryInterface.createTable(USER_STATUS_TABLE, UserStatusSchema);
    await queryInterface.createTable(REFERRAL_STATUS_TABLE, ReferralStatusSchema);
    await queryInterface.createTable(CANDIDATE_STATUS_TABLE, CandidateStatusSchema);
    await queryInterface.createTable(ENGLISH_LEVEL_TABLE, EnglishLevelSchema);
    await queryInterface.createTable(ROLE_TABLE, RoleSchema);
    await queryInterface.createTable(JOB_TABLE, JobSchema);
    await queryInterface.createTable(USER_TABLE, UserSchema);
    await queryInterface.createTable(REFERRAL_TABLE, ReferralSchema);
    await queryInterface.createTable(CANDIDATE_TABLE, CandidateSchema);
  },

  async down (queryInterface) {
    await queryInterface.dropTable(USER_STATUS_TABLE);
    await queryInterface.dropTable(REFERRAL_STATUS_TABLE);
    await queryInterface.dropTable(CANDIDATE_STATUS_TABLE);
    await queryInterface.dropTable(ENGLISH_LEVEL_TABLE);
    await queryInterface.dropTable(ROLE_TABLE);
    await queryInterface.dropTable(JOB_TABLE);
    await queryInterface.dropTable(USER_TABLE);
    await queryInterface.dropTable(REFERRAL_TABLE);
    await queryInterface.dropTable(CANDIDATE_TABLE);
  }
};
