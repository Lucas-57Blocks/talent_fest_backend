const Joi = require('joi');

const id = Joi.number().integer();
const status = Joi.string().min(3).max(20);

const getStatusSchema = Joi.object({
  id: id.required(),
});

const createStatusSchema = Joi.object({
  status: status.required(),
});

const updateStatusSchema = Joi.object({ status });

module.exports = { getStatusSchema, createStatusSchema, updateStatusSchema }
