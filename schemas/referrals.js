const Joi = require('joi');

const id = Joi.number().integer();
const code = Joi.string();
const candidateName = Joi.string().min(3).max(20);
const candidateEmail = Joi.string();
const referrerName = Joi.string().min(3).max(20);
const referrerEmail = Joi.string();
const referralStatusId = Joi.number().integer();

const limit = Joi.number().integer();
const offset = Joi.number().integer();

const getReferralSchema = Joi.object({
  id: id.required(),
});

const createReferralSchema = Joi.object({
  code: code.required(),
  candidateName: candidateName.required(),
  candidateEmail: candidateEmail.required(),
  referrerName: referrerName.required(),
  referrerEmail: referrerEmail.required(),
  referralStatusId: referralStatusId.required(),
});

const updateReferralSchema = Joi.object({
  code,
  candidateName,
  candidateEmail,
  referrerName,
  referrerEmail,
  referralStatusId
});

const queryReferralSchema = Joi.object({
  limit,
  offset,
});

module.exports = { getReferralSchema, createReferralSchema, updateReferralSchema, queryReferralSchema }
