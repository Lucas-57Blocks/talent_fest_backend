const Joi = require('joi');

const id = Joi.number().integer();
const englishLevel = Joi.string().min(3).max(20);

const getEnglishLevelSchema = Joi.object({
  id: id.required(),
});

const createEnglishLevelSchema = Joi.object({
  englishLevel: englishLevel.required(),
});

const updateEnglishLevelSchema = Joi.object({ englishLevel });

module.exports = { getEnglishLevelSchema, createEnglishLevelSchema, updateEnglishLevelSchema }
