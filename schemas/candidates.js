const Joi = require('joi');

const id = Joi.number().integer();
const jobId = Joi.number().integer();
const name = Joi.string().min(3).max(20);
const email = Joi.string().min(3).max(20);
const phone = Joi.string().min(10).max(10);
const linkedIn = Joi.string();
const englishLevelId = Joi.number().integer();
const resume = Joi.string();
const reasonsWhy = Joi.string();
const candidateStatusId = Joi.number().integer();

const limit = Joi.number().integer();
const offset = Joi.number().integer();

const getCandidateSchema = Joi.object({
  id: id.required(),
});

const createCandidateSchema = Joi.object({
  jobId: jobId.required(),
  name: name.required(),
  email: email.required(),
  phone: phone.required(),
  linkedIn: linkedIn.required(),
  englishLevelId: englishLevelId.required(),
  candidateStatusId: candidateStatusId.required(),
});

const updateCandidateSchema = Joi.object({
  jobId,
  name,
  email,
  phone,
  linkedIn,
  englishLevelId,
  resume,
  reasonsWhy,
  candidateStatusId,
});

const queryCandidateSchema = Joi.object({
  limit,
  offset,
  jobId,
  candidateStatusId,
});

module.exports = { getCandidateSchema, createCandidateSchema, updateCandidateSchema, queryCandidateSchema }
