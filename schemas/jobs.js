const Joi = require('joi');

const id = Joi.number().integer();
const name = Joi.string().min(3).max(20);
const summary = Joi.string();
const jobDescription = Joi.string();
const jobDescriptionWishes = Joi.string();
const jobOffer = Joi.string();

const limit = Joi.number().integer();
const offset = Joi.number().integer();

const getJobSchema = Joi.object({
  id: id.required(),
});

const createJobSchema = Joi.object({
  name: name.required(),
  summary: summary.required(),
  jobDescription: jobDescription.required(),
  jobDescriptionWishes: jobDescriptionWishes.required(),
  jobOffer: jobOffer.required(),
});

const updateJobSchema = Joi.object({
  name,
  summary,
  jobDescription,
  jobDescriptionWishes,
  jobOffer,
});

const queryJobSchema = Joi.object({
  limit,
  offset,
});

module.exports = { getJobSchema, createJobSchema, updateJobSchema, queryJobSchema }
