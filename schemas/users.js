const Joi = require('joi');

const id = Joi.number().integer();
const name = Joi.string().min(3).max(20);
const email = Joi.string().min(3).max(20);
const password = Joi.string().min(3).max(20);
const roleId = Joi.number().integer();
const userStatusId = Joi.number().integer();

const limit = Joi.number().integer();
const offset = Joi.number().integer();

const getUserSchema = Joi.object({
  id: id.required(),
});

const createUserSchema = Joi.object({
  name: name.required(),
  email: email.required(),
  password: password.required(),
  roleId: roleId.required(),
  userStatusId: userStatusId.required(),
});

const updateUserSchema = Joi.object({
  name,
  email,
  password,
  roleId,
  userStatusId,
});

const queryUserSchema = Joi.object({
  limit,
  offset,
  roleId,
});

module.exports = { getUserSchema, createUserSchema, updateUserSchema, queryUserSchema }
