const nodemailer = require('nodemailer');

const createTrans = () => {
  return nodemailer.createTransport({
    host: "smtp.mailtrap.io",
    port: 2525,
    auth: {
      user: "3ffeefc6bcb6e7",
      pass: "0d7e9312091bb6"
    }
  });
}

async function sendMail(data) {
  const transport = createTrans();
  const mailInfo = await transport.sendMail({
    from: 'talent_fest@57blocks.com',
    to: data.to,
    subject: data.subject,
    html: data.html
  });
  console.log('Message sent: %s', mailInfo.messageId);
}

module.exports = { sendMail };
