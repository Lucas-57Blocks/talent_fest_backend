const Multer = require('multer');

const maxSize = 2 * 1024 * 1024;

function uploadResume() {
  const storage = Multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, 'uploads');
    },
    filename: (req, file, cb) => {
      const { id } = req.params;
      const fileType = file.originalname.split('.');
      const fileName = id + '_CR.' + fileType[fileType.length-1];
      req.params.resumeFileName = fileName;
      cb(null, fileName);
    }
  });
  const upload = Multer({
    storage: storage,
    limits: { fileSize: maxSize }
  });
  return upload.single('resume');
}

module.exports = { uploadResume };
