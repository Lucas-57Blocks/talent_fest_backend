const express = require('express');
const candidateRouter = require('./candidates');
const candidateStatusRouter = require('./candidate.status');
const englishLevelRouter = require('./english.level');
const jobRouter = require('./jobs');
const referralRouter = require('./referrals');
const referralStatusRouter = require('./referral.status');
const userRouter = require('./users');
const userStatusRouter = require('./user.status');

function routerApi(app) {
  const router = express.Router();
  app.use('/api/v1', router);
  router.use('/candidates', candidateRouter);
  router.use('/candidate-status', candidateStatusRouter);
  router.use('/english-level', englishLevelRouter);
  router.use('/jobs', jobRouter);
  router.use('/referrals', referralRouter);
  router.use('/referral-status', referralStatusRouter);
  router.use('/users', userRouter);
  router.use('/user-status', userStatusRouter);
}

module.exports = routerApi;
