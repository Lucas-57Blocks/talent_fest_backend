const express = require('express');
const ReferralStatusService = require('../services/referral.status');
const { getStatusSchema, createStatusSchema, updateStatusSchema } = require('../schemas/status');
const { validatorHandler } = require('../middlewares/validator.handler');

const router = express.Router();
const service = new ReferralStatusService();

/**
 * @description Get referralStatus
 * @returns {array} - response query postgresDB as array
 */
router.get('/', async (req, res, next) => {
  try {
    res.json(await service.find());
  } catch (error) {
    next(error);
  }
});

/**
 * @description Get referralStatus by ID
 * @param {string} id - URI param
 * @returns {object} - response query postgresDB as object
 */
router.get('/:id',
  validatorHandler(getStatusSchema, 'params'),
  async (req, res, next) => {
    try {
      const { id } = req.params;
      const referralStatus = await service.findOne(id);
      res.json(referralStatus);
    } catch (error) {
      next(error);
    }
  }
);

/**
 * @description Create referralStatus
 * @param {object} body - body request
 * @returns {object} - response query postgresDB as object
 */
router.post('/',
  validatorHandler(createStatusSchema, 'body'),
  async  (req, res, next) => {
    try {
      const body = req.body;
      const newReferralStatus = await service.create(body);
      res.status(201)
        .json({
          message: 'Created',
          data: newReferralStatus
        });
    } catch (error) {
      next(error);
    }
  }
);

/**
 * @description Update referralStatus
 * @param {object} body - body request
 * @returns {object} - response query postgresDB as object
 */
router.patch('/:id',
  validatorHandler(getStatusSchema, 'params'),
  validatorHandler(updateStatusSchema, 'body'),
  async (req, res, next) => {
    try {
      const { id } = req.params;
      const body = req.body;
      const referralStatus = await service.update(id, body);
      res.json({
        message: 'ReferralStatus changed',
        data: referralStatus
      });
    } catch (error) {
      next(error);
    }
  }
);

/**
 * @description Remove referralStatus
 * @param {string} id - URI param
 * @returns {string} - response referralStatus ID removed
 */
router.delete('/:id', async (req, res, next) => {
  try {
    const { id } = req.params;
    const idReferralStatus = await service.delete(id);
    res.json({
      message: 'Deleted',
      idReferralStatus
    });
  } catch (error) {
    next(error);
  }
});

module.exports = router;
