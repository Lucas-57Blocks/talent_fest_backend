const express = require('express');
const UserStatusService = require('./../services/user.status');
const { getStatusSchema, createStatusSchema, updateStatusSchema } = require('./../schemas/status');
const { validatorHandler } = require('./../middlewares/validator.handler');

const router = express.Router();
const service = new UserStatusService();

/**
 * @description Get user status
 * @returns {array} - response query postgresDB as array
 */
router.get('/', async (req, res, next) => {
  try {
    res.json(await service.find());
  } catch (error) {
    next(error);
  }
});

/**
 * @description Get user status by ID
 * @param {string} id - URI param
 * @returns {object} - response query postgresDB as object
 */
router.get('/:id',
  validatorHandler(getStatusSchema, 'params'),
  async (req, res, next) => {
    try {
      const { id } = req.params;
      const userStatus = await service.findOne(id);
      res.json(userStatus);
    } catch (error) {
      next(error);
    }
  }
);

/**
 * @description Create user status
 * @param {object} body - body request
 * @returns {object} - response query postgresDB as object
 */
router.post('/',
  validatorHandler(createStatusSchema, 'body'),
  async  (req, res, next) => {
    try {
      const body = req.body;
      const newUserStatus = await service.create(body);
      res.status(201)
        .json({
          message: 'Created',
          data: newUserStatus
        });
    } catch (error) {
      next(error);
    }
  }
);

/**
 * @description Update user status
 * @param {object} body - body request
 * @returns {object} - response query postgresDB as object
 */
router.patch('/:id',
  validatorHandler(getStatusSchema, 'params'),
  validatorHandler(updateStatusSchema, 'body'),
  async (req, res, next) => {
    try {
      const { id } = req.params;
      const body = req.body;
      const userStatus = await service.update(id, body);
      res.json({
        message: 'User status changed',
        data: userStatus
      });
    } catch (error) {
      next(error);
    }
  }
);

/**
 * @description Remove user status
 * @param {string} id - URI param
 * @returns {string} - response user ID removed
 */
router.delete('/:id', async (req, res, next) => {
  try {
    const { id } = req.params;
    const idUserStatus = await service.delete(id);
    res.json({
      message: 'Deleted',
      idUserStatus
    });
  } catch (error) {
    next(error);
  }
});

module.exports = router;
