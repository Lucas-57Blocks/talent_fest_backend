const express = require('express');
const CandidateService = require('../services/candidates');
const { getCandidateSchema, createCandidateSchema, updateCandidateSchema, queryCandidateSchema } = require('../schemas/candidates');
const { validatorHandler } = require('../middlewares/validator.handler');
const { uploadResume } = require('../middlewares/upload.files');
const Path = require('path');

const router = express.Router();
const service = new CandidateService();

/**
 * @description Get candidates
 * @returns {array} - response query postgresDB as array
 */
router.get('/',
  validatorHandler(queryCandidateSchema, 'query'),
  async (req, res, next) => {
    try {
      res.json(await service.find(req.query));
    } catch (error) {
      next(error);
    }
  }
);

/**
 * @description Get candidate by ID
 * @param {string} id - URI param
 * @returns {object} - response query postgresDB as object
 */
router.get('/:id',
  validatorHandler(getCandidateSchema, 'params'),
  async (req, res, next) => {
    try {
      const { id } = req.params;
      const candidate = await service.findOne(id);
      res.json(candidate);
    } catch (error) {
      next(error);
    }
  }
);

/**
 * @description Download candidate resume
 * @param {string} id - URI param
 * @returns {object} - response candidate resume
 */
router.get('/download-resume/:id',
  validatorHandler(getCandidateSchema, 'params'),
  async (req, res, next) => {
    try {
      const { id } = req.params;
      const { resume } = await service.findOne(id);
      const pathUploads = Path.join(__dirname, '../uploads/');
      res.download(pathUploads+resume, resume, (err) => {
        if (err) {
          res.status(500).send({
            message: "Could not download the file. " + err,
          });
        }
      });
    } catch (error) {
      next(error);
    }
  }
);

/**
 * @description Create candidate
 * @param {object} body - body request
 * @returns {object} - response query postgresDB as object
 */
router.post('/',
  validatorHandler(createCandidateSchema, 'body'),
  async  (req, res, next) => {
    try {
      const body = req.body;
      const newCandidate = await service.create(body);
      res.status(201)
        .json({
          message: 'Created',
          data: newCandidate
        });
    } catch (error) {
      next(error);
    }
  }
);

/**
 * @description Upload candidate reesume
 * @param {object} file - file request
 * @returns {object} - response with upload confirmation
 */
router.post('/upload-resume/:id',
  uploadResume(),
  async (req, res, next) => {
    const { id, resumeFileName } = req.params;
    try {
      const candidate = await service.update(id, {resume: resumeFileName});
      res.json({
          message: 'Resume uploaded',
          candidate
        });
    } catch (error) {
      next(error);
    }
  }
);

/**
 * @description Update candidate
 * @param {object} body - body request
 * @returns {object} - response query postgresDB as object
 */
router.patch('/:id',
  validatorHandler(getCandidateSchema, 'params'),
  validatorHandler(updateCandidateSchema, 'body'),
  async (req, res, next) => {
    try {
      const { id } = req.params;
      const body = req.body;
      const candidate = await service.update(id, body);
      res.json({
        message: 'Candidate changed',
        data: candidate
      });
    } catch (error) {
      next(error);
    }
  }
);

/**
 * @description Remove candidate
 * @param {string} id - URI param
 * @returns {string} - response candidate ID removed
 */
router.delete('/:id', async (req, res, next) => {
  try {
    const { id } = req.params;
    const idCandidate = await service.delete(id);
    res.json({
      message: 'Deleted',
      idCandidate
    });
  } catch (error) {
    next(error);
  }
});

module.exports = router;
