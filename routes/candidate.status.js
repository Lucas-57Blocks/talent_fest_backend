const express = require('express');
const CandidateStatusService = require('../services/candidate.status');
const { getStatusSchema, createStatusSchema, updateStatusSchema } = require('../schemas/status');
const { validatorHandler } = require('../middlewares/validator.handler');

const router = express.Router();
const service = new CandidateStatusService();

/**
 * @description Get candidateStatus
 * @returns {array} - response query postgresDB as array
 */
router.get('/', async (req, res, next) => {
  try {
    res.json(await service.find());
  } catch (error) {
    next(error);
  }
});

/**
 * @description Get candidateStatus by ID
 * @param {string} id - URI param
 * @returns {object} - response query postgresDB as object
 */
router.get('/:id',
  validatorHandler(getStatusSchema, 'params'),
  async (req, res, next) => {
    try {
      const { id } = req.params;
      const candidateStatus = await service.findOne(id);
      res.json(candidateStatus);
    } catch (error) {
      next(error);
    }
  }
);

/**
 * @description Create candidateStatus
 * @param {object} body - body request
 * @returns {object} - response query postgresDB as object
 */
router.post('/',
  validatorHandler(createStatusSchema, 'body'),
  async  (req, res, next) => {
    try {
      const body = req.body;
      const newCandidateStatus = await service.create(body);
      res.status(201)
        .json({
          message: 'Created',
          data: newCandidateStatus
        });
    } catch (error) {
      next(error);
    }
  }
);

/**
 * @description Update candidateStatus
 * @param {object} body - body request
 * @returns {object} - response query postgresDB as object
 */
router.patch('/:id',
  validatorHandler(getStatusSchema, 'params'),
  validatorHandler(updateStatusSchema, 'body'),
  async (req, res, next) => {
    try {
      const { id } = req.params;
      const body = req.body;
      const candidateStatus = await service.update(id, body);
      res.json({
        message: 'CandidateStatus changed',
        data: candidateStatus
      });
    } catch (error) {
      next(error);
    }
  }
);

/**
 * @description Remove candidateStatus
 * @param {string} id - URI param
 * @returns {string} - response candidateStatus ID removed
 */
router.delete('/:id', async (req, res, next) => {
  try {
    const { id } = req.params;
    const idCandidateStatus = await service.delete(id);
    res.json({
      message: 'Deleted',
      idCandidateStatus
    });
  } catch (error) {
    next(error);
  }
});

module.exports = router;
