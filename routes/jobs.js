const express = require('express');
const JobService = require('../services/jobs');
const { getJobSchema, createJobSchema, updateJobSchema, queryJobSchema } = require('../schemas/jobs');
const { validatorHandler } = require('../middlewares/validator.handler');

const router = express.Router();
const service = new JobService();

/**
 * @description Get jobs
 * @returns {array} - response query postgresDB as array
 */
router.get('/',
  validatorHandler(queryJobSchema, 'query'),
  async (req, res, next) => {
    try {
      res.json(await service.find(req.query));
    } catch (error) {
      next(error);
    }
  }
);

/**
 * @description Get job by ID
 * @param {string} id - URI param
 * @returns {object} - response query postgresDB as object
 */
router.get('/:id',
  validatorHandler(getJobSchema, 'params'),
  async (req, res, next) => {
    try {
      const { id } = req.params;
      const job = await service.findOne(id);
      res.json(job);
    } catch (error) {
      next(error);
    }
  }
);

/**
 * @description Create job
 * @param {object} body - body request
 * @returns {object} - response query postgresDB as object
 */
router.post('/',
  validatorHandler(createJobSchema, 'body'),
  async  (req, res, next) => {
    try {
      const body = req.body;
      const newJob = await service.create(body);
      res.status(201)
        .json({
          message: 'Created',
          data: newJob
        });
    } catch (error) {
      next(error);
    }
  }
);

/**
 * @description Update job
 * @param {object} body - body request
 * @returns {object} - response query postgresDB as object
 */
router.patch('/:id',
  validatorHandler(getJobSchema, 'params'),
  validatorHandler(updateJobSchema, 'body'),
  async (req, res, next) => {
    try {
      const { id } = req.params;
      const body = req.body;
      const job = await service.update(id, body);
      res.json({
        message: 'Job changed',
        data: job
      });
    } catch (error) {
      next(error);
    }
  }
);

/**
 * @description Remove job
 * @param {string} id - URI param
 * @returns {string} - response user ID removed
 */
router.delete('/:id', async (req, res, next) => {
  try {
    const { id } = req.params;
    const idJob = await service.delete(id);
    res.json({
      message: 'Deleted',
      idJob
    });
  } catch (error) {
    next(error);
  }
});

module.exports = router;
