const express = require('express');
const ReferralService = require('../services/referrals');
const { getReferralSchema, createReferralSchema, updateReferralSchema, queryReferralSchema } = require('../schemas/referrals');
const { validatorHandler } = require('../middlewares/validator.handler');

const router = express.Router();
const service = new ReferralService();

/**
 * @description Get referrals
 * @returns {array} - response query postgresDB as array
 */
router.get('/',
  validatorHandler(queryReferralSchema, 'query'),
  async (req, res, next) => {
    try {
      res.json(await service.find(req.query));
    } catch (error) {
      next(error);
    }
  }
);

/**
 * @description Get referral by ID
 * @param {string} id - URI param
 * @returns {object} - response query postgresDB as object
 */
router.get('/:id',
  validatorHandler(getReferralSchema, 'params'),
  async (req, res, next) => {
    try {
      const { id } = req.params;
      const referral = await service.findOne(id);
      res.json(referral);
    } catch (error) {
      next(error);
    }
  }
);

/**
 * @description Create referral
 * @param {object} body - body request
 * @returns {object} - response query postgresDB as object
 */
router.post('/',
  validatorHandler(createReferralSchema, 'body'),
  async  (req, res, next) => {
    try {
      const body = req.body;
      const newReferral = await service.create(body);
      res.status(201)
        .json({
          message: 'Created',
          data: newReferral
        });
    } catch (error) {
      next(error);
    }
  }
);

/**
 * @description Update referral
 * @param {object} body - body request
 * @returns {object} - response query postgresDB as object
 */
router.patch('/:id',
  validatorHandler(getReferralSchema, 'params'),
  validatorHandler(updateReferralSchema, 'body'),
  async (req, res, next) => {
    try {
      const { id } = req.params;
      const body = req.body;
      const referral = await service.update(id, body);
      res.json({
        message: 'Referral changed',
        data: referral
      });
    } catch (error) {
      next(error);
    }
  }
);

/**
 * @description Remove referral
 * @param {string} id - URI param
 * @returns {string} - response referral ID removed
 */
router.delete('/:id', async (req, res, next) => {
  try {
    const { id } = req.params;
    const idReferral = await service.delete(id);
    res.json({
      message: 'Deleted',
      idReferral
    });
  } catch (error) {
    next(error);
  }
});

module.exports = router;
