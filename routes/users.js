const express = require('express');
const UserService = require('./../services/users');
const { getUserSchema, createUserSchema, updateUserSchema, queryUserSchema } = require('./../schemas/users');
const { validatorHandler } = require('./../middlewares/validator.handler');

const router = express.Router();
const service = new UserService();

/**
 * @description Get users
 * @returns {array} - response query postgresDB as array
 */
router.get('/',
  validatorHandler(queryUserSchema, 'query'),
  async (req, res, next) => {
    try {
      res.json(await service.find(req.query));
    } catch (error) {
      next(error);
    }
  }
);

/**
 * @description Get user by ID
 * @param {string} id - URI param
 * @returns {object} - response query postgresDB as object
 */
router.get('/:id',
  validatorHandler(getUserSchema, 'params'),
  async (req, res, next) => {
    try {
      const { id } = req.params;
      const user = await service.findOne(id);
      res.json(user);
    } catch (error) {
      next(error);
    }
  }
);

/**
 * @description Create user
 * @param {object} body - body request
 * @returns {object} - response query postgresDB as object
 */
router.post('/',
  validatorHandler(createUserSchema, 'body'),
  async  (req, res, next) => {
    try {
      const body = req.body;
      const newUser = await service.create(body);
      res.status(201)
        .json({
          message: 'Created',
          data: newUser
        });
    } catch (error) {
      next(error);
    }
  }
);

/**
 * @description Update user
 * @param {object} body - body request
 * @returns {object} - response query postgresDB as object
 */
router.patch('/:id',
  validatorHandler(getUserSchema, 'params'),
  validatorHandler(updateUserSchema, 'body'),
  async (req, res, next) => {
    try {
      const { id } = req.params;
      const body = req.body;
      const user = await service.update(id, body);
      res.json({
        message: 'User changed',
        data: user
      });
    } catch (error) {
      next(error);
    }
  }
);

/**
 * @description Remove user
 * @param {string} id - URI param
 * @returns {string} - response user ID removed
 */
router.delete('/:id', async (req, res, next) => {
  try {
    const { id } = req.params;
    const idUser = await service.delete(id);
    res.json({
      message: 'Deleted',
      idUser
    });
  } catch (error) {
    next(error);
  }
});

module.exports = router;
