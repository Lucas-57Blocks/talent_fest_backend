const express = require('express');
const EnglishLevelService = require('../services/english.level');
const { getEnglishLevelSchema, createEnglishLevelSchema, updateEnglishLevelSchema } = require('../schemas/english.level');
const { validatorHandler } = require('../middlewares/validator.handler');

const router = express.Router();
const service = new EnglishLevelService();

/**
 * @description Get englishLevel
 * @returns {array} - response query postgresDB as array
 */
router.get('/', async (req, res, next) => {
  try {
    res.json(await service.find());
  } catch (error) {
    next(error);
  }
});

/**
 * @description Get englishLevel by ID
 * @param {string} id - URI param
 * @returns {object} - response query postgresDB as object
 */
router.get('/:id',
  validatorHandler(getEnglishLevelSchema, 'params'),
  async (req, res, next) => {
    try {
      const { id } = req.params;
      const englishLevel = await service.findOne(id);
      res.json(englishLevel);
    } catch (error) {
      next(error);
    }
  }
);

/**
 * @description Create englishLevel
 * @param {object} body - body request
 * @returns {object} - response query postgresDB as object
 */
router.post('/',
  validatorHandler(createEnglishLevelSchema, 'body'),
  async  (req, res, next) => {
    try {
      const body = req.body;
      const newEnglishLevel = await service.create(body);
      res.status(201)
        .json({
          message: 'Created',
          data: newEnglishLevel
        });
    } catch (error) {
      next(error);
    }
  }
);

/**
 * @description Update englishLevel
 * @param {object} body - body request
 * @returns {object} - response query postgresDB as object
 */
router.patch('/:id',
  validatorHandler(getEnglishLevelSchema, 'params'),
  validatorHandler(updateEnglishLevelSchema, 'body'),
  async (req, res, next) => {
    try {
      const { id } = req.params;
      const body = req.body;
      const englishLevel = await service.update(id, body);
      res.json({
        message: 'EnglishLevel changed',
        data: englishLevel
      });
    } catch (error) {
      next(error);
    }
  }
);

/**
 * @description Remove englishLevel
 * @param {string} id - URI param
 * @returns {string} - response englishLevel ID removed
 */
router.delete('/:id', async (req, res, next) => {
  try {
    const { id } = req.params;
    const idEnglishLevel = await service.delete(id);
    res.json({
      message: 'Deleted',
      idEnglishLevel
    });
  } catch (error) {
    next(error);
  }
});

module.exports = router;
