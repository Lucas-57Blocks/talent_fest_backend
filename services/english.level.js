const boom = require('@hapi/boom');
const { models } = require('../libs/sequelize');

class EnglishLevelService {

  async create(data) {
    return await models.EnglishLevel.create(data);
  }
  async find() {
    let options = { where: { isDeleted: false } };
    return await models.EnglishLevel.findAll(options);
  }
  async findOne(id) {
    const englishLevel = await models.EnglishLevel.findByPk(id);
    if(!englishLevel) throw boom.notFound('EnglishLevel not found');
    if(englishLevel.isDeleted) throw boom.conflict('EnglishLevel is deleted.');
    return englishLevel;
  }
  async update(id, changes) {
    const englishLevel = await this.findOne(id);
    return await englishLevel.update(changes);
  }
  async delete(id) {
    const englishLevel = await this.findOne(id);
    if(!englishLevel) throw boom.notFound('EnglishLevel not found');
    await englishLevel.update({ isDeleted: true });
    return { id };
  }

}

module.exports = EnglishLevelService;
