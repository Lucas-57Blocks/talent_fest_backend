const boom = require('@hapi/boom');
const { models } = require('./../libs/sequelize');

class UserService {

  async create(data) {
    return await models.User.create(data);
  }
  async find(query) {
    let options = {
      where: { isDeleted: false }
    }
    const { limit, offset, role } = query;
    if (limit) options.limit = limit;
    if (offset) options.offset = offset;
    if (role) options.where.role = role;
    return await models.User.findAll(options);
  }
  async findOne(id) {
    let options = { include: ['role', 'user_status'] };
    const user = await models.User.findByPk(id, options);
    if(!user) throw boom.notFound('User not found');
    if(user.isDeleted) throw boom.conflict('User is deleted.');
    return user;
  }
  async update(id, changes) {
    const user = await this.findOne(id);
    return await user.update(changes);
  }
  async delete(id) {
    const user = await this.findOne(id);
    if(!user) throw boom.notFound('User not found');
    await user.update({ isDeleted: true });
    return { id };
  }

}

module.exports = UserService;
