const boom = require('@hapi/boom');
const { models } = require('../libs/sequelize');
const { mailTemplates } = require('../utils/constants');
const { sendMail } = require('../libs/emailer');

class ReferralService {

  async create(data) {
    const referral = await models.Referral.create(data);
    if(referral) {
      const mailInfo = {
        to: [referral.candidateEmail, referral.referrerEmail],
        subject: mailTemplates.SUBJECT,
        html: mailTemplates.NOTIFY_BEFORE_CODE + referral.code + mailTemplates.NOTIFY_AFTER_CODE
      }
      sendMail(mailInfo);
    }
    return referral;
  }
  async find(query) {
    let options = { where: { isDeleted: false } };
    const { limit, offset } = query;
    if (limit) options.limit = limit;
    if (offset) options.offset = offset;
    const referrals = await models.Referral.findAll(options);
    return referrals;
  }
  async findOne(id) {
    const referral = await models.Referral.findByPk(id);
    if(!referral) throw boom.notFound('Referral not found');
    if(referral.isDeleted) throw boom.conflict('Referral is deleted.');
    return referral;
  }
  async update(id, changes) {
    const referral = await this.findOne(id);
    return await referral.update(changes);
  }
  async delete(id) {
    const referral = await this.findOne(id);
    if(!referral) throw boom.notFound('Referral not found');
    await referral.update({ isDeleted: true });
    return { id };
  }

}

module.exports = ReferralService;
