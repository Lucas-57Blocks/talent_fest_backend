const boom = require('@hapi/boom');
const { models } = require('../libs/sequelize');

class JobService {

  async create(data) {
    return await models.Job.create(data);
  }
  async find(query) {
    let options = {
      where: { isDeleted: false, isHidden: false }
    }
    const { limit, offset, } = query;
    if (limit) options.limit = limit;
    if (offset) options.offset = offset;
    return await models.Job.findAll(options);
  }
  async findOne(id) {
    const job = await models.Job.findByPk(id);
    if(!job) throw boom.notFound('Job not found.');
    if(job.isDeleted) throw boom.conflict('Job is deleted.');
    return job;
  }
  async update(id, changes) {
    const job = await this.findOne(id);
    return await job.update(changes);
  }
  async delete(id) {
    const job = await this.findOne(id);
    if(!job) throw boom.notFound('Job not found.');
    await job.update({ isDeleted: true });
    return { id };
  }

}

module.exports = JobService;
