const boom = require('@hapi/boom');
const { models } = require('../libs/sequelize');

class UserStatusService {
  async create(data) {
    return await models.UserStatus.create(data);
  }
  async find() {
    let options = { where: { isDeleted: false } };
    return await models.UserStatus.findAll(options);
  }
  async findOne(id) {
    const user = await models.UserStatus.findByPk(id);
    if(!user) throw boom.notFound('UserStatus not found');
    if(user.isDeleted) throw boom.conflict('UserStatus is deleted.');
    return user;
  }
  async update(id, changes) {
    const user = await this.findOne(id);
    return await user.update(changes);
  }
  async delete(id) {
    const user = await this.findOne(id);
    if(!user) throw boom.notFound('User not found');
    await user.update({ isDeleted: true });
    return { id };
  }

}

module.exports = UserStatusService;
