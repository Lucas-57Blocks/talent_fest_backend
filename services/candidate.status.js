const boom = require('@hapi/boom');
const { models } = require('../libs/sequelize');

class CandidateStatusService {

  async create(data) {
    return await models.CandidateStatus.create(data);
  }
  async find() {
    let options = { where: { isDeleted: false } };
    return await models.CandidateStatus.findAll(options);
  }
  async findOne(id) {
    const candidateStatus = await models.CandidateStatus.findByPk(id);
    if(!candidateStatus) throw boom.notFound('CandidateStatus not found');
    if(candidateStatus.isDeleted) throw boom.conflict('CandidateStatus is deleted.');
    return candidateStatus;
  }
  async update(id, changes) {
    const candidateStatus = await this.findOne(id);
    return await candidateStatus.update(changes);
  }
  async delete(id) {
    const candidateStatus = await this.findOne(id);
    if(!candidateStatus) throw boom.notFound('CandidateStatus not found');
    await candidateStatus.update({ isDeleted: true });
    return { id };
  }

}

module.exports = CandidateStatusService;
