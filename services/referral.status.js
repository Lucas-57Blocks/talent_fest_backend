const boom = require('@hapi/boom');
const { models } = require('../libs/sequelize');

class ReferralStatusService {

  async create(data) {
    return await models.ReferralStatus.create(data);
  }
  async find() {
    let options = { where: { isDeleted: false } };
    return await models.ReferralStatus.findAll(options);
  }
  async findOne(id) {
    const referralStatus = await models.ReferralStatus.findByPk(id);
    if(!referralStatus) throw boom.notFound('ReferralStatus not found');
    if(referralStatus.isDeleted) throw boom.conflict('ReferralStatus is deleted.');
    return referralStatus;
  }
  async update(id, changes) {
    const referralStatus = await this.findOne(id);
    return await referralStatus.update(changes);
  }
  async delete(id) {
    const referralStatus = await this.findOne(id);
    if(!referralStatus) throw boom.notFound('ReferralStatus not found');
    await referralStatus.update({ isDeleted: true });
    return { id };
  }

}

module.exports = ReferralStatusService;
