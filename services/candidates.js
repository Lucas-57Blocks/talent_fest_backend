const boom = require('@hapi/boom');
const { models } = require('../libs/sequelize');

class CandidateService {

  async create(data) {
    return await models.Candidate.create(data);
  }
  async find(query) {
    let options = { where: { isDeleted: false } };
    const { limit, offset, jobId, candidateStatusId } = query;
    if (limit) options.limit = limit;
    if (offset) options.offset = offset;
    if (jobId) options.where.jobId = jobId;
    if (candidateStatusId) options.where.candidateStatusId = candidateStatusId;
    return await models.Candidate.findAll(options);
  }
  async findOne(id) {
    let options = { include: ['job', 'english_level', 'candidate_status'] }
    const candidate = await models.Candidate.findByPk(id, options);
    if(!candidate) throw boom.notFound('Candidate not found');
    if(candidate.isDeleted) throw boom.conflict('Candidate is deleted.');
    return candidate;
  }
  async update(id, changes) {
    const candidate = await this.findOne(id);
    return await candidate.update(changes);
  }
  async delete(id) {
    const candidate = await this.findOne(id);
    if(!candidate) throw boom.notFound('Candidate not found');
    await candidate.update({ isDeleted: true });
    return { id };
  }

}

module.exports = CandidateService;
